﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebBanHang.Models;
using WebBanHang.Repositories;

namespace WebBanHang.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = SD.Role_Admin)]
    public class ProductController : Controller
    {
        private readonly IProductRepository _productRepository;
        private readonly ICategoryRepository _categoryRepository;


        public IActionResult Index()
        {
            return View();
        }
        [Authorize(Roles = SD.Role_Admin)] // Ví dụ: chỉ có người dùng với quyền Role_Manager mới có thể truy cập action này
        public IActionResult ManageProducts()
        {
            // Logic xử lý khi người dùng truy cập vào action ManageProducts ở đây
            return View();
        }
    }
}
