﻿namespace WebBanHang.Models
{
    public class ShoppingCart
    {
        public List<CartItem> Items { get; set; } = new List<CartItem>();
        public void AddItem(CartItem item)
        {
            var existingItem = Items.FirstOrDefault(i => i.ProductId ==
            item.ProductId);
            if (existingItem != null)
            {
                existingItem.Quantity += item.Quantity;
            }
            else
            {
                Items.Add(item);
            }
        }
        public void RemoveItem(int productId)
        {
            Items.RemoveAll(i => i.ProductId == productId);
        }
        public int TotalQuantity()
        {
            return Items.Sum(i => i.Quantity);
        }
        public decimal TotalPrice()
        {
            return Items.Sum(i => i.Price * i.Quantity);
        }
        public void Clear()
        {
            Items.Clear();
        }
        public CartItem GetItemById(int productId)
        {
            return Items.FirstOrDefault(i => i.ProductId == productId);
        }
        public void UpdateQuantity(int productId, int newQuantity)
        {
            var item = Items.FirstOrDefault(i => i.ProductId == productId);
            if (item != null)
            {
                item.Quantity = newQuantity;
            }
            else
            {
                // Handle case where item with given productId is not found
                // For example, you can throw an exception to indicate the item was not found.
                throw new InvalidOperationException($"Item with productId {productId} was not found in the shopping cart.");
                // Alternatively, you can handle it in a different way based on your requirements.
            }
        }
        public bool IsEmpty()
        {
            return Items.Count == 0;
        }

    }
}